var _WM_APP_PROPERTIES = {
  "activeTheme" : "material",
  "dateFormat" : "",
  "defaultLanguage" : "en",
  "displayName" : "PassportAPP1",
  "homePage" : "Main",
  "name" : "PassportAPP1",
  "platformType" : "WEB",
  "supportedLanguages" : "en",
  "timeFormat" : "",
  "type" : "APPLICATION",
  "version" : "1.0"
};